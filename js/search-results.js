function $_GET(param) {
	var vars = {};
	window.location.href.replace( location.hash, '' ).replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function( m, key, value ) { // callback
			vars[key] = value !== undefined ? value : '';
		}
	);

	if ( param ) {
		return vars[param] ? vars[param] : null;	
	}
	return vars;
}

function searchOnLoad(){
if($_GET('q') !== null){
document.getElementById("search-term-header").innerHTML = 'Search Results: ' + $_GET('q').replace('%20', ' ');
var s = $_GET('q');
if (console.log) console.log(s);
	s.trim();
	if( s === "Plymouth"){
		$( "#plymouth-results" ).css('display', 'block');
		$( "#peter-andre-results" ).css('display', 'none');
	}else if( s === "Peter%20Andre" || s === "Peter" || s === "Andre" || s === "Peter Andre"){
		$( "#peter-andre-results" ).css('display', 'block');
		$( "#plymouth-results" ).css('display', 'none');
	}else{
		$( "#peter-andre-results" ).css('display', 'none');
		$( "#plymouth-results" ).css('display', 'none');
	}
}else{
	$( "#peter-andre-results" ).css('display', 'none');
	$( "#plymouth-results" ).css('display', 'none');
}
	getUser();
}